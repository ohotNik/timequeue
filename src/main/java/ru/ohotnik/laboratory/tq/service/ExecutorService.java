package ru.ohotnik.laboratory.tq.service;

import ru.ohotnik.laboratory.tq.SendingMessage;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ExecutorService {

  private final ScheduledExecutorService executor;

  public ExecutorService(final ScheduledExecutorService executor) {
    this.executor = executor;
  }

  public void receiveMessage(final SendingMessage msg) {
    long delay = ChronoUnit.MILLIS.between(LocalDateTime.now(), msg.getLocalDateTime());
    //noinspection unchecked
    executor.schedule(msg.getCommand(), delay, TimeUnit.MILLISECONDS);
  }

}
