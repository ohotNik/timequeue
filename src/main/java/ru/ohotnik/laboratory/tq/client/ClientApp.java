package ru.ohotnik.laboratory.tq.client;

import ru.ohotnik.laboratory.tq.SendingMessage;
import ru.ohotnik.laboratory.tq.SerializableCallable;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.security.SecureRandom;
import java.time.LocalDateTime;

import static java.time.temporal.ChronoUnit.MILLIS;
import static java.time.temporal.ChronoUnit.SECONDS;

/**
 * @author Okhonchenko Aleksander
 * @since 25.06.2018
 */
public class ClientApp {

  public static void main(String[] args) throws IOException, InterruptedException {
    final InetSocketAddress address = new InetSocketAddress( 9005);
    final SocketChannel channel = SocketChannel.open(address);

    final SecureRandom random = new SecureRandom();
    for (int i = 0; i < 1000; i++) {
      SendingMessage<String> m = new SendingMessage<>();
      LocalDateTime localDateTime = LocalDateTime.now()
          .plus(random.nextInt(10000), MILLIS)
          .plus(5, SECONDS);
      m.setLocalDateTime(localDateTime);
      m.setCommand(new SerializableCallable(localDateTime));

      ByteArrayOutputStream out = new ByteArrayOutputStream();
      ObjectOutputStream objectOutputStream
          = new ObjectOutputStream(out);
      objectOutputStream.writeObject(m);
      objectOutputStream.flush();
      objectOutputStream.close();

      channel.write(ByteBuffer.wrap(out.toByteArray()));
      Thread.sleep(500 + random.nextInt(1000));
    }

    channel.close();
  }

}
