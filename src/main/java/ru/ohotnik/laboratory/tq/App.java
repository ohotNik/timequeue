package ru.ohotnik.laboratory.tq;

import ru.ohotnik.laboratory.tq.service.ExecutorService;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ScheduledThreadPoolExecutor;

public class App {

  public static void main(String[] args) throws IOException, ClassNotFoundException {
    final Selector selector = Selector.open();
    final ServerSocketChannel channel = ServerSocketChannel.open();
    final InetSocketAddress address = new InetSocketAddress(9005);
    channel.bind(address);
    channel.configureBlocking(false);
    final int ops = channel.validOps();
    channel.register(selector, ops, null);

    final ExecutorService service = new ExecutorService(new ScheduledThreadPoolExecutor(1));

    while (true) {
      selector.select();
      final Set<SelectionKey> keys = selector.selectedKeys();
      final Iterator<SelectionKey> iterator = keys.iterator();
      while (iterator.hasNext()) {
        final SelectionKey k = iterator.next();
        if (k.isAcceptable()) {
          final SocketChannel sc = channel.accept();
          sc.configureBlocking(false);
          sc.register(selector, SelectionKey.OP_READ);
        } else if (k.isReadable()) {
          final SocketChannel selectableChannel = (SocketChannel) k.channel();
          final ByteBuffer buffer = ByteBuffer.allocate(2048);
          selectableChannel.read(buffer);
          final byte[] array = buffer.array();
          if (array.length == 0) {
            selectableChannel.close();
          }
          ObjectInputStream objectInputStream
              = new ObjectInputStream(new ByteArrayInputStream(array));
          //noinspection unchecked
          SendingMessage<String> message = (SendingMessage<String>) objectInputStream.readObject();
          service.receiveMessage(message);
        }
      }
      iterator.remove();
    }
  }
}
