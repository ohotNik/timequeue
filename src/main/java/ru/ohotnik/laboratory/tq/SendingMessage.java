package ru.ohotnik.laboratory.tq;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.concurrent.Callable;

public class SendingMessage<T> implements Serializable {

  private LocalDateTime localDateTime;

  private Callable<T> command;

  public LocalDateTime getLocalDateTime() {
    return localDateTime;
  }

  public void setLocalDateTime(LocalDateTime localDateTime) {
    this.localDateTime = localDateTime;
  }

  public Callable<T> getCommand() {
    return command;
  }

  public void setCommand(Callable<T> command) {
    this.command = command;
  }
}
