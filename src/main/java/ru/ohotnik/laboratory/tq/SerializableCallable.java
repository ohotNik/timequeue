package ru.ohotnik.laboratory.tq;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.concurrent.Callable;

public class SerializableCallable implements Callable<String>, Serializable {

  private LocalDateTime time;

  public SerializableCallable() {
  }

  public SerializableCallable(final LocalDateTime localDateTime) {
    this.time = localDateTime;
  }

  @Override
  public String call() throws Exception {
    System.out.println("Time : " + LocalDateTime.now() + " | " + time.toString());
    return "hello";
  }
}
